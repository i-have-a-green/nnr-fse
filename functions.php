<?php
/**
 * Functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Armando
 * @since 1.0.0
 */

/**
 * The theme version.
 *
 * @since 1.0.0
 */
define( 'IHAG_VERSION', wp_get_theme()->get( 'Version' ) );

/** Check if the WordPress version is 5.5 or higher, and if the PHP version is at least 7.2. If not, do not activate. */
if ( version_compare( $GLOBALS['wp_version'], '5.5', '<' ) || version_compare( PHP_VERSION_ID, '70200', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

/**
 * Adds theme-supports.
 *
 * @since 1.2.4
 * @return void
 */
function armando_setup() {
	// Add support for Block Styles.
	add_theme_support( 'wp-block-styles' );
	// Enqueue editor styles.
	add_theme_support( 'editor-styles' );

	add_theme_support( 'custom-units');

	// Add support for full and wide align images.
	add_theme_support( 'align-wide' );

	add_editor_style(
/* 		array(
			'./assets/css/style-shared.css',
		) */
		"style.css"
	);


	/*
	* Enable support for Post Thumbnails on posts and pages.
	*
	* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	*/
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1568, 9999 );
	add_image_size( '150-150', 100, 100 );
	add_image_size( '350-350', 350, 350 );
	add_image_size( '450-450', 450, 450 );
	add_image_size( '650-650', 650, 650 );
	add_image_size( '850-850', 850, 850 );

	function ihag_custom_sizes( $sizes ) {
		return array_merge(
			$sizes,
			array(
				'150-150' => __( '150x150 - Petite sans rognage', 'ihag' ),
				'350-350' => __( '350x350 - Contact sans rognage', 'ihag' ),
				'450-450' => __( '450x450 - Moyen sans rognage', 'ihag' ),
				'650-650' => __( '650x650 - Grande sans rognage', 'ihag' ),
				'650-650' => __( '850x850 - Grande sans rognage', 'ihag' ),
			)
		);
	}
	add_filter( 'image_size_names_choose', 'ihag_custom_sizes' );


	// Add support for full and wide align images.
	add_theme_support( 'align-wide' );

	// Add support for editor styles.
	add_theme_support( 'editor-styles' );
}
add_action( 'after_setup_theme', 'armando_setup' );

/**
 * Enqueue the style.css file.
 *
 * @since 1.0.0
 */

/**
 * Show '(No title)' if post has no title.
 */
add_filter(
	'the_title',
	function( $title ) {
		if ( ! is_admin() && empty( $title ) ) {
			$title = __( '(No title)', 'armando' );
		}

		return $title;
	}
);


add_action( 'init', 'ihag_custom_post_property' );
function ihag_custom_post_property() {

	
// CPT Ressource :

	register_post_type(
		'ressource', 
		array(
			'labels'             => array(
				'name'          => __( 'Ressources', 'ihag' ),
				'singular_name' => __( 'ressource', 'ihag' ),
			),
			'menu_position'      => 18,
			'menu_icon'          => 'dashicons-portfolio',
			'hierarchical'       => false,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'ressources' ),
			'show_in_rest'       => true,
			'has_archive'        => true,
			'supports'           => array( 'title', 'revisions', 'author' )
		)
	);

// TAXO CPT Ressource:

	$args = array(
		'label'        => __( 'Ressources-taxo', 'ihag' ),
		'hierarchical' => true
	);
	register_taxonomy( 'ressource-taxo', 'ressource', $args );
	

	register_post_type(
		'jobboard', 
		array(
			'labels'             => array(
				'name'          => __( 'Jobboard', 'ihag' ),
				'singular_name' => __( 'jobboard', 'ihag' ),
			),
			'menu_position'      => 18,
			'menu_icon'          => 'dashicons-portfolio',
			'hierarchical'       => false,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'jobboard' ),
			'show_in_rest'       => true,
			'has_archive'        => true,
			'supports'           => array( 'title', 'editor', 'thumbnail', 'revisions', 'excerpt' ),
		)
	);

// TAXO CPT Ressource:

	$args = array(
		'label'        => __( 'jobboard-taxo', 'ihag' ),
		'hierarchical' => true
	);
	register_taxonomy( 'jobboard-taxo', 'jobboard', $args );
}



function myplugin_register_template() {
    $post_type_object = get_post_type_object( 'ressource' );
    $post_type_object->template = array(
        array( 'core/image' ),
    );
}
add_action( 'init', 'myplugin_register_template' );

/**
 * Propose les plugins à télécharger.
 */
require get_template_directory() . '/inc/plugin-require.php';


// Variantes de styles
function ihag_register_styles() {
	/*
	EXEMPLE STRUCTURE DÉCLARATION STYLE DE BLOC GUT :

	register_block_style(
		'core/bloc',
		array(
			'name'	        => 'slug',
			'label'	        => 'name'
		)
	); 
	*/

}

add_action( 'init', 'ihag_register_styles' );

/**
 *  ACF Field.
 */
require get_template_directory() . '/inc/acf-block.php';

/**
 *  ACF Field styles.
 */
require get_template_directory() . '/inc/acf-style.php';

/**
 * Enqueue scripts and styles.
 */
function ihag_scripts() {
	wp_enqueue_script( 'ihag-color-script', get_template_directory_uri() . '/js/color.js', array(), IHAG_VERSION, true );
	wp_enqueue_style( 'ihag-style', get_stylesheet_uri(), array(), IHAG_VERSION );

	wp_enqueue_script( 'ihag-script', get_template_directory_uri() . '/js/script.js', array(), IHAG_VERSION, true );
	wp_localize_script('ihag-script', 'resturl', site_url() . '/wp-json/ihag/');
	wp_localize_script( 'ihag-script', 'wpApiSettings', array(
		'root' => esc_url_raw( rest_url() ),
		'nonce' => wp_create_nonce( 'wp_rest' )
	) );

}
add_action( 'wp_enqueue_scripts', 'ihag_scripts' );
/**
 * My_acf_json_save_point
 *
 * @param  mixed $path
 * @return string
 */
function ihag_acf_json_save_point( $path ) {
	$path = get_stylesheet_directory() . '/jsonACF';
	if ( ! file_exists( $path ) ) {
		mkdir( $path, 0777 );}
	return $path;
}
add_filter( 'acf/settings/save_json', 'ihag_acf_json_save_point' );


/**
 * My_acf_json_load_point
 *
 * @param  mixed $paths
 * @return string
 */
function ihag_acf_json_load_point( $paths ) {
	unset( $paths[0] );
	$paths[] = get_stylesheet_directory() . '/jsonACF';
	return $paths;
}
add_filter( 'acf/settings/load_json', 'ihag_acf_json_load_point' );


if ( ! defined( 'WP_POST_REVISIONS' ) ) {
	define( 'WP_POST_REVISIONS', 3 );
}


/**
 * Ihag_revision_number
 *
 * @param  mixed $num
 * @param  mixed $post
 * @return int
 */
function ihag_revision_number( $num, $post ) {
	return 3;
}
add_filter( 'wp_revisions_to_keep', 'ihag_revision_number', 4, 2 );

// un intervalle entre deux sauvegardes de 360 secondes.
if ( ! defined( 'AUTOSAVE_INTERVAL' ) ) {
	define( 'AUTOSAVE_INTERVAL', 360 );
}

/**
 * Ihag_clean_head
 * 
 * @link https://crunchify.com/how-to-clean-up-wordpress-header-section-without-any-plugin/
 * 
 * @return void
 */
function ihag_clean_head() {
	return '';
}
add_filter( 'the_generator', 'ihag_clean_head' );

remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'wp_shortlink_wp_head' );
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds.
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed.

/**
 * Ihag_title_cat
 *
 * @param  mixed $title
 * @return string
 */
function ihag_title_cat( $title ) {
	if ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$title = single_tag_title( '', false );
	} elseif ( is_author() ) {
		$title = get_the_author();
	} elseif ( is_tax() ) { 
		$title = single_term_title( '', false );
	} elseif ( is_post_type_archive() ) {
		$title = post_type_archive_title( '', false );
	}
	return $title;
}
add_filter( 'get_the_archive_title', 'ihag_title_cat' );
