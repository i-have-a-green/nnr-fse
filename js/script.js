document.addEventListener('DOMContentLoaded', function() {
    //Fix current active item menu
    if (document.body.classList.contains("post-type-archive-ressource")) {
        let elements = document.getElementsByClassName('archive-ressource');
        if (elements[0]) {
            elements[0].classList.add('current-menu-item');
        }

        //Gestion filtres responsive
        let filtersToggle = document.getElementsByClassName('filters-toggle');
        let postTagsFilters = document.getElementsByClassName('post-tags-filters');
        
        Array.prototype.forEach.call(filtersToggle, function (el, i) {
        
            el.addEventListener('click',function () {
                postTagsFilters[i].classList.toggle('filters-toggle-active');
            });
        
        });
    }

    //Fix current active item menu
    else if (document.body.classList.contains("post-type-archive-jobboard")) {
        let elements = document.getElementsByClassName('archive-jobboard');
        if (elements[0]) {
            elements[0].classList.add('current-menu-item');
        }

        //Gestion filtres responsive
        let filtersToggle = document.getElementsByClassName('filters-toggle');
        let postTagsFilters = document.getElementsByClassName('post-tags-filters');
        
        Array.prototype.forEach.call(filtersToggle, function (el, i) {
        
            el.addEventListener('click',function () {
                postTagsFilters[i].classList.toggle('filters-toggle-active');
            });
        
        });
    }
    
    //Fix current active item menu
    else if (document.body.classList.contains("single-jobboard")) {
        let elements = document.getElementsByClassName('archive-jobboard');
        if (elements[0]) {
            elements[0].classList.add('current-menu-item');
        }
    }
});