document.addEventListener('DOMContentLoaded', function() {

    let root = document.documentElement;

    // color template = ["main-color", "light-color", "dark-color", "background-color"];
    let colorPink = ["#F4DAFC", "#f5e3fa", "#E5AAFA", "#f5ebf7"];
    let colorBlue = ["#DADDFC", "#EEEEFE", "#A8B0F9", "#f6f6ff"];
    let colorOrange = ["#ffd683", "#FFDF9E", "#ffbc35", "#fff2d8"];

    let colors = [colorPink, colorBlue, colorOrange];
    i = Math.floor(Math.random() * 3);
    
    root.style.setProperty('--color__primary', colors[i][0]);
    root.style.setProperty('--color__light__primary', colors[i][1]);
    root.style.setProperty('--color__dark__primary', colors[i][2]);
    root.style.setProperty('--color__bg', colors[i][3]);

});