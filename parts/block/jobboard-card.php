<?php
/**
 * Template part for displaying jobboard posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ihag
 */


?>
<article id="jobboard-card-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="post-thumbnail">
        <?php
            echo '<a href="' .  esc_url( get_permalink( $post->ID ) ) . '" rel="bookmark">', the_post_thumbnail( '' ), '</a>';
        ?>
    </div>
    <div class="jobboard-post-heading">
        <h2>
            <?php
            the_title( '<h2><a href="'. esc_url( get_permalink( $post->ID ) ) .'" rel="bookmark">', '</a></h2>' );
            ?>
        </h2>
    </div>
    <?php
        $terms = get_the_terms( $post, 'jobboard-taxo' );
        if ( $terms ) {
        echo '<div class="post-tags">';
        foreach ( $terms as $taxonomy ) :
            ?>
                <a href="<?php echo esc_url( get_term_link( $taxonomy ) ); ?>"><?php echo esc_attr( $taxonomy->name ); ?></a>
            <?php 
        endforeach;
        echo '</div>';
    }
    ?>
    <div class="jobboard-post-date">
        <time datetime="<?php echo get_the_date( 'Y-m-d' ); ?>">Publié le <?php echo get_the_date(); ?></time>
    </div>
    <div class="post-link">
        <a class="post_link" href="<?php echo esc_url( get_permalink( $post->ID ) ); ?>">Voir l'offre d'emploi ></a>
    </div>

</article><!-- #post-<?php the_ID(); ?> -->