<?php
/**
 * Block name: Listing ressource
 */
$term_taxonomy = get_queried_object(); 
?>

<div class="breadcrumb_container">
    <a href="<?php echo get_home_url();?>">Accueil</a>
    <span>➞ </span>
    <a href="<?php echo get_post_type_archive_link('ressource')?>">Ressources</a>
    <?php if (is_tax()) { ?>
        <span>➞ </span>
        <span><?php echo get_queried_object()->name;?></span>
    <?php }; ?>
</div>

<div class="archive-title">
    <h1>Ressources</h1>
    <svg id="deco_h1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 621.12 72.73"><defs></defs><g data-name="Layer 2"><g data-name="Layer 1"><polygon class="cls-1" points="466.72 72.73 358.72 18.73 313.72 72.73 205.72 18.73 160.72 72.73 52.72 18.73 11.52 68.17 0 58.56 48.8 0 156.8 54 201.8 0 309.8 54 354.8 0 462.8 54 507.8 0 621.12 56.66 614.41 70.07 511.72 18.73 466.72 72.73"></polygon></g></g></svg>
</div>

<div class="filters">
    <p class="filters-toggle"><?php _e('Filtres', 'ihag'); ?></p>

    <?php
        $terms = get_terms( array( 'taxonomy' => 'ressource-taxo' ) );
        if ( $terms ) {
            echo '<div class="post-tags-filters">';
            ?>
            <a href="<?php echo home_url('/ressources'); ?>" <?php echo $active; ?>><?php _e( 'Tous', 'ihag' ); ?></a>
            <?php    
            foreach ( $terms as $term ) :
                $active = '';
                if($term_taxonomy && $term_taxonomy->term_id == $term->term_id){
                    $active = ' class="active"';
                }
                ?>
                    <a href="<?php echo esc_url( get_term_link( $term ) ); ?>" <?php echo $active;?>><?php echo esc_attr( $term->name ); ?></a>
                <?php 
            endforeach;
            echo '</div>';
        }
    ?>
</div>

<div class="ressource-postContainer">
        <?php
            $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

            if(is_tax() && $term_taxonomy){
                $args = array(
                    'paged'             => $paged,
                    'post_type'         => 'ressource',
                    'post_status'       => 'publish',
                    'posts_per_page'    =>  9,
                    'tax_query'         => array(
                        array(
                            'taxonomy' => 'ressource-taxo',
                            'field'    => 'slug',
                            'terms'    => $term_taxonomy->slug,
                        )
                    )
                );
            }
            else{
                $args = array(
                    'paged'             => $paged,
                    'post_type'         => 'ressource',
                    'post_status'       => 'publish',
                    'posts_per_page'    =>  9
                );
            }

            //global $post;
            query_posts( $args );
            if ( have_posts() ) {
                while ( have_posts() ) { the_post();
                    //setup_postdata($post);
                    get_template_part( 'parts/block/ressource-card', get_post_type() );
                }
            }
            //wp_reset_postdata();
            
            
        ?>
</div>

<div class="pagination">
    <div class="pagination__prev">
        <?php previous_posts_link( '‹ Publications précédentes' ); ?>
    </div>
    <div class="pagination__next">
        <?php next_posts_link( 'Publications suivantes ›' ); ?> 
    </div>
</div>