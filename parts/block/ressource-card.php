<?php
/**
 * Template part for displaying ressource posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ihag
 */


?>
<article id="ressource-card-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="ressource-post-heading">
        <h2>
            <?php
            the_title();
            ?>
        </h2>
    </div>
    <?php
        $terms = get_the_terms( $post, 'ressource-taxo' );
        if ( $terms ) {
        echo '<div class="post-tags">';
        foreach ( $terms as $taxonomy ) :
            ?>
                <a href="<?php echo esc_url( get_term_link( $taxonomy ) ); ?>"><?php echo esc_attr( $taxonomy->name ); ?></a>
            <?php 
        endforeach;
        echo '</div>';
    }
    ?>
    <div class="ressource-post-date">
        <time datetime="<?php echo get_the_date( 'Y-m-d' ); ?>">Publié le <?php echo get_the_date(); ?></time>
    </div>
    <div class="post-link">
        <a class="post_link" href="<?php the_field('link', get_the_id())?>" target="_blank">Voir la ressource ></a>
    </div>

</article><!-- #post-<?php the_ID(); ?> -->