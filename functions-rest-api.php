<?php
/*
WP_REST_Server::READABLE = ‘GET’
WP_REST_Server::EDITABLE = ‘POST’
WP_REST_Server::DELETABLE = ‘DELETE’
WP_REST_Server::CREATE = ‘PUT’
*/

/*
add_action('rest_api_init', function() {
	register_rest_route( 'ihag', 'action-rest',
		array(
		'methods' 				=> 'POST', 
		'callback'        		=> 'ihagCallback'
		)
	);
});
function ihagCallback(WP_REST_Request $request){
	if ( check_nonce() ) {
		$params = $request->get_params();
		return new WP_REST_Response( 'Hello World', 200 );
	}
	return new WP_REST_Response( 'BAD NONCE', 401 );
}
*/

function check_nonce(){
	global $wp_rest_auth_cookie;
	/*
	 * Is cookie authentication being used? (If we get an auth
	 * error, but we're still logged in, another authentication
	 * must have been used.)
	 */
	if ( true !== $wp_rest_auth_cookie && is_user_logged_in() ) {
		return false;
	}
	// Is there a nonce?
	$nonce = null;
	if ( isset( $_REQUEST['_wp_rest_nonce'] ) ) {
		$nonce = $_REQUEST['_wp_rest_nonce'];
	} elseif ( isset( $_SERVER['HTTP_X_WP_NONCE'] ) ) {
		$nonce = $_SERVER['HTTP_X_WP_NONCE'];
	}
	if ( null === $nonce ) {
		// No nonce at all, so act as if it's an unauthenticated request.
		wp_set_current_user( 0 );
		return false;
	}
	// Check the nonce.
	return wp_verify_nonce( $nonce, 'wp_rest' );
}