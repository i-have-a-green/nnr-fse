<?php
/**
 * Ihag_acf_styles
 *
 */

function ihag_register_acf_styles() {

		/*   
		EXEMPLE STRUCTURE STYLE BLOC CUSTOM ACF :
		
		register_block_style(
			'acf/le-slug-bloc',
			array(
				'name'	        => 'Slug du style pour class',
				'label'	        => 'Nom du style back office'
			)
		); 
		*/
}

add_action( 'init', 'ihag_register_acf_styles' );
?>