<?php

add_action('acf/init', 'acf_init_blocs');
function acf_init_blocs() {
    if( function_exists('acf_register_block_type') ) {
        
        acf_register_block_type(
            array(
                'name'				    => 'svg-raw-n',
                'title'				    => __('Affichage SVG déco N'),
                'description'		    => __('Affichage SVG déco N'),
                'render_template'	    => 'parts/block/svg-raw-n.php',
                'mode'                  => 'preview',
                'icon'				    => 'superhero',
                'keywords'			    => array('Svg', 'Brut'),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                                        )
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'svg-raw-r',
                'title'				    => __('Affichage SVG déco R'),
                'description'		    => __('Affichage SVG déco R'),
                'render_template'	    => 'parts/block/svg-raw-r.php',
                'mode'                  => 'preview',
                'icon'				    => 'superhero',
                'keywords'			    => array('Svg', 'Brut'),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                                        )
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'archive-ressource',
                'title'				    => __('Listing ressource'),
                'description'		    => __('Listing ressource'),
                'render_template'	    => 'parts/block/archive-ressource.php',
                'mode'                  => 'preview',
                'icon'				    => 'superhero',
                'keywords'			    => array('post', 'ressource', 'template'),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'archive-jobboard',
                'title'				    => __('Listing jobboard'),
                'description'		    => __('Listing jobboard'),
                'render_template'	    => 'parts/block/archive-jobboard.php',
                'mode'                  => 'preview',
                'icon'				    => 'superhero',
                'keywords'			    => array('post', 'jobboard', 'template'),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );

    }
}
